package com.example.user.bookmanager.tools;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.widget.Toast;

import com.example.user.bookmanager.entities.Book;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class ControllerBooks extends DataBaseHandler {

    DateConverter dateConverter = new DateConverter();

    public ControllerBooks(Context context) {
        super(context);
    }

    public boolean create(Book bookObj){
        ContentValues contentValues = new ContentValues();

        contentValues.put("title", bookObj.getTitle());
        contentValues.put("author", bookObj.getAuthor());
        contentValues.put("total", bookObj.getNoOfPages());
        contentValues.put("lastpage", bookObj.getLastPage());


        SQLiteDatabase db = this.getWritableDatabase();

        boolean createSuccessful = db.insert("books", null, contentValues) > 0;
        db.close();

        return createSuccessful;
    }

    public List<Book> getBooks() {

        List<Book> dbBooks = new ArrayList<>();
        String sql = "SELECT * FROM books";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, null);

        if (cursor.moveToFirst()){
            do {
                int id = Integer.parseInt(cursor.getString(cursor.getColumnIndex("id")));
                String title = cursor.getString(cursor.getColumnIndex("title"));
                String author = cursor.getString(cursor.getColumnIndex("author"));
                int total = Integer.parseInt(cursor.getString(cursor.getColumnIndex("total")));
                int lastPage = Integer.parseInt(cursor.getString(cursor.getColumnIndex("lastpage")));


                Book book = new Book(title, author, total, lastPage);
                book.setId(id);
                dbBooks.add(book);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return dbBooks;

    }

    public int count() {

        SQLiteDatabase db = this.getWritableDatabase();

        String sql = "SELECT * FROM books";
        int recordCount = db.rawQuery(sql, null).getCount();
        db.close();

        return recordCount;

    }
}

