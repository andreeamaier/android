package com.example.user.bookmanager.activityrelated;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.bookmanager.R;
import com.example.user.bookmanager.entities.Book;
import com.example.user.bookmanager.tools.ControllerBooks;
import com.example.user.bookmanager.tools.DateConverter;
import com.example.user.bookmanager.tools.DatePickerFragment;

public class NewBookActivity extends AppCompatActivity implements DatePickerFragment.OnDateChangeListenerInterface{

    private TextView dateShow;
    private Book newBook;
    private EditText titleEdit, authorEdit, pagesEdit, lastPageEdit;
    private String title, author, date;
    private int totalPages = 0 , lastPage = 0;
    private Context context;
    private DateConverter dateConverter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO: 17-Jan-19 de vazut care-i treapa cu "App" si cum se leaga object box sau alta db 

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_book);

        Button startDateButton;
        Button addBookButton;

        startDateButton = findViewById(R.id.start_date);
        titleEdit = findViewById(R.id.title_field);
        authorEdit = findViewById(R.id.author_field);
        pagesEdit = findViewById(R.id.total_field);
        lastPageEdit = findViewById(R.id.last_page_field);
        dateShow = findViewById(R.id.date);
        addBookButton = findViewById(R.id.add_update_button);

        addBookButton.setText(getResources().getString(getResources().getIdentifier("addButton","string",getPackageName())));

        startDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               displayDate();
            }
        });

        addBookButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                context = v.getContext();
                getEnteredData();
                Book newBook = new Book(title,author,totalPages,lastPage);
                boolean createSuccessful = new ControllerBooks(context).create(newBook);

                if (createSuccessful) {
                    Toast.makeText(context, "Book added.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, "Unable to save the book.", Toast.LENGTH_SHORT).show();
                }

                Intent intent = new Intent(NewBookActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

    }

    private void getEnteredData() {
        title = titleEdit.getText().toString();
        author = authorEdit.getText().toString();
        String totalPagesStr = pagesEdit.getText().toString();
        String lastPageStr = lastPageEdit.getText().toString();
        date = dateShow.getText().toString();

        if (isInteger(totalPagesStr)){
            totalPages = Integer.parseInt(totalPagesStr);
        }
        if (isInteger(lastPageStr)){
            lastPage = Integer.parseInt(lastPageStr);
        }
    }

    private boolean isInteger( String input ) {
        try {
            Integer.parseInt( input );
            return true;
        }
        catch( Exception e ) {
            return false;
        }
    }

    @Override
    public void onDateChangeListener(String date) {
        dateShow.setText(date);
    }

    public void displayDate() {
        DialogFragment datePickerFragment = new DatePickerFragment();
        datePickerFragment.show(getSupportFragmentManager(), "datePicker");
    }

}



