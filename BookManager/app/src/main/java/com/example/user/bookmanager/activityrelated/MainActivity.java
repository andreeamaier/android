package com.example.user.bookmanager.activityrelated;

import android.app.Application;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.user.bookmanager.R;
import com.example.user.bookmanager.entities.Book;
import com.example.user.bookmanager.tools.ControllerBooks;
import com.example.user.bookmanager.tools.MyAdapter;


import java.util.List;

import io.objectbox.BoxStore;


public class MainActivity extends AppCompatActivity {

    private Button newBookButton;
    private MyAdapter mAdapter;
    private LinearLayoutManager mLinearLayoutManager;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // do this once, for example in your Application class


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        countRecords();

        displayRecords();

        newBookButton = findViewById(R.id.add_new_book);
        newBookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, NewBookActivity.class);
                startActivity(intent);
            }
        });
       }

    public void countRecords() {

        int recCount = new ControllerBooks(this).count();
        TextView recordCountTextView = findViewById(R.id.recordCount);
        if (recCount == 1) {
            recordCountTextView.setText("You are reading " + recCount + " book at the moment.");
        } else {
            recordCountTextView.setText("You are reading " + recCount + " books at the moment.");
        }

    }

    public void displayRecords() {

        RecyclerView recyclerView = findViewById(R.id.rec_view);
        recyclerView.removeAllViews();

        List<Book> books = new ControllerBooks(this).getBooks();

        recyclerView.setHasFixedSize(true);
        mAdapter = new MyAdapter (books);
        recyclerView.setAdapter(mAdapter);
        mLinearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLinearLayoutManager);

        }

    }



