package com.example.user.bookmanager.tools;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.user.bookmanager.R;
import com.example.user.bookmanager.entities.Book;

import java.util.List;


public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private List<Book> mDataset;


    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView titleTV, authorTV, totalTV, lastpageTV;

        public MyViewHolder(View v) {
            super(v);
            titleTV = v.findViewById(R.id.bookTitle);
            authorTV = v.findViewById(R.id.bookAuthor);
            totalTV = v.findViewById(R.id.totalPages);
            lastpageTV = v.findViewById(R.id.lastPage);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public MyAdapter(List<Book> myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MyAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        // create a new view
        Context context = parent.getContext();
        View v = LayoutInflater.from(context)
                .inflate(R.layout.row_layout, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        Book book = mDataset.get(position);
        holder.titleTV.setText(book.getTitle());
        holder.authorTV.setText(book.getAuthor());
        holder.totalTV.setText(Integer.toString(book.getNoOfPages()));
        holder.lastpageTV.setText(Integer.toString(book.getLastPage()));

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
