package com.example.user.bookmanager.tools;

import android.os.Build;
import android.support.annotation.RequiresApi;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateConverter {

    @RequiresApi(api = Build.VERSION_CODES.O)
    public LocalDateTime stringToLocalDate (String str) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d-MM-yyyy");
        LocalDateTime ldt =  LocalDateTime.parse(str, formatter);

        return ldt;

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public String ldtToString (LocalDateTime ldt) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d-MM-yyyy");
        String str = ldt.format(formatter);

        return str;

    }

}
