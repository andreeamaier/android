package com.example.user.bookmanager.tools;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "BookDatabase";

    public DataBaseHandler(Context context){
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String sql = "CREATE TABLE books " +
                "( id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "title TEXT, " + "author TEXT, " +
                "total INTEGER, " + "lastpage INTEGER )";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        String sql = "DROP TABLE IF EXISTS books";
        db.execSQL(sql);
        onCreate(db);
    }
}
